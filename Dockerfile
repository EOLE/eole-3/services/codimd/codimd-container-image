ARG CODIMD_RELEASE=master
ARG CODIMD_REPO_URL=https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/codimd-sources.git
ARG NODE_VERSION=16.20.2-bullseye
ARG USER_NAME=hackmd
ARG UID=1500
ARG GID=1500
ARG PORTCHECKER_VERSION=v1.1.0

#------------------------------------------------------------------------
FROM node:${NODE_VERSION} as PGCLIENT
ARG USER_NAME
ARG UID
ARG GID

ENV DEBIAN_FRONTEND=noninteractive

RUN set -xe && \
        apt-get update && \
        # install postgres client
        apt-get install -y --no-install-recommends apt-transport-https postgresql-client && \
        # Add user and groupd
        groupadd --gid $GID $USER_NAME && \
        useradd --uid $UID --gid $GID --no-log-init --create-home $USER_NAME

#------------------------------------------------------------------------
FROM PGCLIENT as BUILDPACK
ARG USER_NAME
ARG UID
ARG GID

RUN set -xe && \
        apt-get update && \
        apt-get install -y git && \
        # install node-prune
        npm i -g node-prune && npm cache clean --force && \
        mkdir -p /home/$USER_NAME/.npm && \
        echo "prefix=/home/$USER_NAME/.npm/" > /home/$USER_NAME/.npmrc && \
        # setup github ssh key
        mkdir -p /home/$USER_NAME/.ssh && \
        # ssh-keyscan -H github.com >> /home/$USER_NAME/.ssh/known_hosts && \
        # setup git credential helper
        mkdir -p /home/$USER_NAME/git && \
        git config --global credential.helper 'store --file /home/$USER_NAME/git/credentials' && \
        # adjust permission
        chown -R $USER_NAME:$USER_NAME /home/$USER_NAME

#------------------------------------------------------------------------
FROM BUILDPACK as BUILD
ARG CODIMD_RELEASE
ARG CODIMD_REPO_URL
ARG USER_NAME
ARG UID
ARG GID

ENV QT_QPA_PLATFORM=offscreen

# Commands should be run as user
USER ${USER_NAME}
ENV PATH="/home/${USER_NAME}/.npm/bin:$PATH"

RUN git clone -b ${CODIMD_RELEASE} ${CODIMD_REPO_URL} /home/${USER_NAME}/app
WORKDIR /home/$USER_NAME/app

RUN set -xe && \
    git reset --hard && \
    git clean -fx && \
    npm install && \
    npm run build && \
    cp ./deployments/docker-entrypoint.sh ./ && \
    cp .sequelizerc.example .sequelizerc && \
    rm -rf .git .gitignore .travis.yml .dockerignore .editorconfig .babelrc .mailmap .sequelizerc.example \
        test docs contribute \
        package-lock.json webpack.prod.js webpack.htmlexport.js webpack.dev.js webpack.common.js \
        config.json.example README.md CONTRIBUTING.md AUTHORS node_modules

#------------------------------------------------------------------------
FROM PGCLIENT as RUNTIME

ENV NODE_ENV=production
ARG PORTCHECKER_VERSION

RUN apt-get update && \
    apt-get install fonts-noto-core -y && \
    rm -rf /var/lib/apt/lists/*

RUN set -xe && \
        # install pchecker
        wget https://github.com/hackmdio/portchecker/releases/download/${PORTCHECKER_VERSION}/portchecker-linux-$(dpkg --print-architecture).tar.gz && \
        tar xvf portchecker-linux-$(dpkg --print-architecture).tar.gz -C /usr/local/bin && \
        mv /usr/local/bin/portchecker-linux-$(dpkg --print-architecture) /usr/local/bin/pcheck && \
        rm portchecker-linux-$(dpkg --print-architecture).tar.gz && \
        # rebuild fontconfig cache
        fc-cache -f -v && \
        dpkg-reconfigure fontconfig-config && \
        dpkg-reconfigure fontconfig

#------------------------------------------------------------------------
FROM RUNTIME
ARG USER_NAME
ARG UID
ARG GID

ENV QT_QPA_PLATFORM=offscreen

COPY --from=BUILD --chown=${USER_NAME}:${USER_NAME} /home/${USER_NAME}/app /home/${USER_NAME}/app
COPY --chown=${USER_NAME}:${USER_NAME} features.md /home/${USER_NAME}/app/public/docs

# Commands should be run as user
USER ${USER_NAME}
WORKDIR /home/$USER_NAME/app
ENV PATH="/home/${USER_NAME}/.npm/bin:$PATH"

RUN npm install --production && npm cache clean --force && rm -rf /tmp/{core-js-banners,phantomjs}
EXPOSE 3000
ENTRYPOINT "${HOME}/app/docker-entrypoint.sh"
