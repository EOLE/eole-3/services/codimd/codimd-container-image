# <center> <i class="fa fa-gear fa-spin" style="color: firebrick"></i> Tutoriel CodiMD

</center>

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_6e91626de00442b592198b400e6b284d.png =120x50)


Ceci est un résumé des fonctionnalités présentées en anglais [dans cette page](https://demo.codimd.org/s/features).
Ce document a été traduit par @bertrand.chartier.

## :one: Introduction

:pencil: CodiMD est une plateforme d'écriture collaborative, accessible à travers tous les navigateurs. Elle est basée sur la syntaxe Markdown , et la mise en forme peut être également faite à l'aide d'une barre d'outils.
Pour sélectionner le mode d'affichage utiliser les boutons: ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_7b746ac98e3383608673edfecf289d50.png =90x27)

:lock: Le créateur peut décider qui peut modifier, qui peut consulter ce document avec le bouton !![](https://minio.apps.education.fr/codimd-prod/uploads/upload_47239c1d5cd66625ba07d6e7bb2e1056.png).
:::spoiler Afficher le tableau des droits
|                              |Propriétaire lect/écrit|Authentifié lecture|Authentifié écriture|Invité lecture|Invité écriture|
|:---------------|:--------------:|:------------:|:-------------:|:--------:|:-----------:|
|<span class="text-nowrap"><i class="fa fa-leaf fa-fw"></i> **Freely**</span>|✔|✔|✔|✔|✔|
|<span class="text-nowrap"><i class="fa fa-pencil fa-fw"></i> **Editable**</span>|✔|✔|✔|✔|✖|
|<span class="text-nowrap"><i class="fa fa-id-card fa-fw"></i> **Limited**</span>|✔|✔|✔|✖|✖|
|<span class="text-nowrap"><i class="fa fa-lock fa-fw"></i> **Locked**</span>|✔|✔|✖|✔|✖|
|<span class="text-nowrap"><i class="fa fa-umbrella fa-fw"></i> **Protected**</span>|✔|✔|✖|✖|✖|
|<span class="text-nowrap"><i class="fa fa-hand-stop-o fa-fw"></i> **Private**</span>|✔|✖|✖|✖|✖|

**Seul le créateur peut modifier les permissions.**
:::
<br>

:desktop_computer: Plusieurs formes de visualisation du document peuvent être obtenues à l'aide du menu : ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_d4c7956710e032e8b170280905e05ecf.png)


:::spoiler syntaxe du mode présentation
>Commencer le document par:
>\-&#8239;-&#8239;-
>type: slide
>slideOptions:
>  transition: slide
>\-&#8239;-&#8239;-
>
> Le spérateur de diapositive est \-&#8239;-&#8239;-
:::
<br>

## :two: Mise en forme
### Titres et sommaire
Il y a 6 niveaux de titres disponibles. Ils vont générer un sommaire automatique (3 premiers niveaux visibles directement).
Pour définir un titre, vous pouvez utiliser le bouton **H** (cliquer plusieurs fois pour obtenir les niveaux de sous-titres), ou le précéder d'un ou plusieurs **#** (leur nombre définiera le niveau du titre).

---

### Attributs de caractère
Le texte sera en **gras** avec le bouton **B** ou entre deux doubles \**, \__.
Le texte sera en _italique_ avec le bouton __*I*__ ou entre deux \*, \_.
Le texte sera ~~barré~~ avec le bouton **~~S~~** ou entre deux doubles \~~.
Le texte sera en ++souligné++ entre deux doubles \++.
Le texte sera en ^exposant^ entre deux \^.
Le texte sera en ~indice~ entre deux \~.
Le texte sera ==surligné== entre deux doubles \==.


Pour afficher le <span style="color: red;">texte en rouge</span>, il faudra l'encadrer par des balises \<span style="color: red;">mon texte\</span>.


La mise en forme de citation se fait avec le bouton <i class="fa fa-quote-right" aria-hidden="true"></i> ou en la précédant d'un \>, suivi d'un choix éventuel de couleur.

---

### Liens et ancres
Une adresse internet sera transformée automatiquement en lien.
Vous pouvez créer des liens à l'aide du bouton <i class="fa fa-link" aria-hidden="true"></i> ou en tapant entre crochet le texte du lien suivi entre parenthèse de l'adresse du lien.
>[color=red] \[GOOGLE](https://google.com) affichera [GOOGLE](https://google.com)

Pour faire référence à un titre du document il suffit d'indiquer comme adresse # suivi de son nom
> \[Début](#-Tutoriel-CodiMD-) affichera [Début](#-Tutoriel-CodiMD-)
>
<br>

Pour ajouter une note de bas de page taper entre crochets une référence puis à un autre endroit du document la même référence suivi de **:** et le contenu de la note.
> note\[\^1] &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \[^1]\: contenu de la note

affichera: note[^1]
[^1]: contenu de la note


---

### Emojis :smiley: et Font awesome   ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_a02e65de9bb0425b192bf4667b90bd3f.png)



Pour afficher des Emojis, taper leur nom entre deux **\:**. Par exemple \:coffee: affiche :coffee:. La liste complète des emojis est [disponible ici](https://www.webfx.com/tools/emoji-cheat-sheet/).

Pour afficher des icônes de fontawesome 4.7, il faut coller le code obtenu dans [fontawesome.com](https://fontawesome.com/v4.7.0/icons/). Par exemple \<i class="fa fa-snowflake-o">\</i> affiche : <i class="fa fa-snowflake-o"></i>.
Pour les icônes de fontawesome 5, vous pouvez télécharger le [fichier svg](https://fontawesome.com/icons?d=gallery) et l'ajouter en tant qu'image.

---

### Listes

Vous pouvez générer des listes à puces ou numérotées à l'aide des deux boutons ou en précédent les items d'un numéro suivi d'un point ou d'une *, +, -.
>1. Premier item
>2. deuxième item:
>    * Sous-item 1
>    * Sous-item 2

Mais également des cases à cocher avec le bouton ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_4d43579e5df2259ac65c3bf10b1dc891.png)( - \[ ] pour une case vide et - \[x] pour une case cochée).

>- [ ] A faire
>- [x] OK

---

### Tableaux
Pour créer des tableaux, il faut utiliser le bouton <i class="fa fa-table" aria-hidden="true"></i>
 ou directement encadrer le nom des colonnes par |. La dimension s'adaptera au contenu.
Une ligne de -&#8239;-&#8239;- apparaitra pour symboliser la séparation entre la ligne des titres et les lignes de cotenus.
Pour gérér l'alignement d'une colonne:
:-&#8239;-&#8239;- à gauche,&nbsp;&nbsp;&nbsp;:-&#8239;-&#8239;-: centré,&nbsp;&nbsp;&nbsp; -&#8239;-&#8239;-: à droite

<center>

>| Colonne1     |  Colonne 2   |     Colonne 3 |
>|:------------ |:------------:| -------------:|
>| :arrow_left: | :arrow_down: | :arrow_right: |
</center>

---

### Encadrés
Pour faire des cadres, il faut utiliser le triple **\:::** associé aux mots clés <span style="color: green;">**success** (cadre vert)</span>, <span style="color: red;">**danger**(cadre rouge)</span>,<span style="color: gold;"> **warning**(cadre jaune)</span> et <span style="color: blue;">**info**(cadre bleu)</span>.
Lorsqu'il est associé à spoiler, il peut afficher un élément à déplier.

>:::success
>::: spoiler Déplier
>:::danger
>:+1:
>:::

<br>

## :three: Intégration images, vidéos,...
 Il est possible d'intégrer de nombreux types de ressources: images, sons, vidéos, pdf, graphiques, maths, partitions, ...

 ### :sunrise: Images
 Pour insérer une image, utiliser le bouton ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_cf378dd29d1eba077d3c92d3458b5e8c.png)
  ou faites glisser/déposer l'image à l'emplacement souhaité. Sinon taper \![ ](adresse de l'image).
Pour spécifier la taille image rajouter **=LxH** ou seulement **=Lx** après le nom de l'image (avec L largeur et H hauteur).

---

### <i class="fa fa-youtube-play" aria-hidden="true" style="color:red"></i>  Youtube et ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_a6c274cc514c74dca0fc89b0fcaead90.png) Peertube

 Pour incruster une video youtube, il faut écrire l'identifiant de la vidéo entre **\{%youtube** et **\%}**.
 {%youtube HUBNt18RFbo %}

  Pour les vidéos peertube, il faut copier/coller le code d'intégration.

 <iframe title="RESNAIS, Oncle Amérique, bureau" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/7GzyoTrkx6CJvqJzbJwPJt" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>
 ---

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers PDF
Pour incruster un document PDF, il faut écrire l'adresse du fichier PDF entre **\{%pdf** et **\%}**.
{%pdf https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf %}

---

### ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_64e284d7de89b06f27ba63788986ee9f.png) Maths
Les formules mathématiques **L^A^T~E~X** peuvent s'afficher en les encadrants entre deux \$ ou deux doubles  \$\$.

---

### ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_452f17d580391bfbfaa99d07d6fbe09f.png) code
Un programme informatique peut être mis en forme entre deux &#96; ou deux triples &#96;&#96;&#96; suivi éventuellement du nom du language, = pour la numérotation des lignes

``` python=
 print("Hello World")
 ```
---

### :chart_with_upwards_trend: Graphiques
Plusieurs types de graphiques peuvent être intégrés : [Flow Charts](http://adrai.github.io/flowchart.js/), [Graphviz](http://www.tonyballantyne.com/graphs.html/), [mermaid](http://knsv.github.io/mermaid), [sequence diagrams](http://bramp.github.io/js-sequence-diagrams/), [vega](https://vega.github.io/vega-lite/docs)

```vega
{
  "$schema": "https://vega.github.io/schema/vega-lite/v4.json",
  "data": {"url": "https://vega.github.io/editor/data/barley.json"},
  "mark": "bar",
  "encoding": {
    "x": {"aggregate": "sum", "field": "yield", "type": "quantitative"},
    "y": {"field": "variety", "type": "nominal"},
    "color": {"field": "site", "type": "nominal"}
  }
}
```
---

:musical_note: Il est possible d'intégrer une partition avec la [syntaxe abc](http://abcnotation.com/learn) :
```abc
X:1
M:4/4
K:G
|:GABc dedB|
```

<div style="float:right;text-align:right;font-size=footnote;"><font size="1"> bertrand.chartier at ac-grenoble.fr </font></div>



